/*
*autor: Kevin Figueroa
*/


// url donde se expone el servidor de la aplicacion

var url='http://localhost:8080/WebServer/rest/tipoEstadoReserva';

/*
    con fetch se accede al path del sever que posee todos los registros,
    se llama a la parte del contenido en la tabla para que se dibuje con todos 
    los registros
*/
function findAll(){
    fetch(url+"/all")
    .then((respuesta) => respuesta.json())
    .then(datos => {
        tabla(datos)
    })

}
/*
    mediante fetch se accede al path del server donde se muestra un rango de valores
    por default (estos valores se pueden modificar agregando parametros )
*/
function findRange(){
    fetch(url).then((rando) => rando.json())
    .then((resp)=>{
       tabla(resp)
    })
}

/*
    funcion que ejecula el metodo POST para guardar registros,
    --resive como parametros la URL donde se expone el metodo
    y el objeto a persistir
*/
function create_(url,entity){
    fetch(url,{
        method  : 'POST',
        body    : JSON.stringify(entity),
        headers : {
            'content-type': 'aplication/json; charset=UTF-8'
        }

    }).then(resp => resp.json())
    .catch(error => console.error("error",error))
    .then(response => console.log("succes",response));
}

/*
    funcion que ejecula el metodo PUT para modificar registros,
    --resive como parametros la URL donde se expone el metodo
    y el objeto a modificar
*/
function edit_(url,entity){
    console.log(entity)
    fetch(url,{
        method  : 'PUT',
        body    : JSON.stringify(entity),
        headers : {
            'Accept': 'application/json',
            'content-type': 'aplication/json'
        }

    }).then(resp => resp.json())
    .catch(error => console.error("error",error))
    .then(response => console.log("succes",response));
}

/*
    funcion que ejecula el metodo DELETE para remover registros,
    --resive como parametros la URL donde se expone el metodo
    y el identificador de la entidad a remover
*/

function eliminar_(url,id){
        return fetch(url + '/' + id, {
          method: 'DELETE'
        })
        .then(resp => resp.json()) 
        .catch(error => console.error("error",error))
    .then(response => console.log("succes",response));  
}