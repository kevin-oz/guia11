/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.rest.server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoEstadoReservaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@Path("tipoEstadoReserva")
@RequestScoped
public class TipoEstadoReservaResource implements Serializable {

    @EJB
    TipoEstadoReservaFacadeLocal tipoEstadoReserva;

    /**
     * Contar registros
     *
     * @return el numero de registros que hay en la base de datos
     */
    @GET
    @Path("count")
    @Produces({MediaType.APPLICATION_JSON})
    public Response count() {
        if (tipoEstadoReserva != null) {
            try {
                int totalRegistros = tipoEstadoReserva.count();
                if (totalRegistros != 0) {
                    return Response.status(Response.Status.OK)
                            .entity(totalRegistros).build();
                }
            } catch (Exception e) {
            }

        }
        return Response.status(Response.Status.NOT_FOUND)
                .header("No se encontraron Registros", tipoEstadoReserva)
                .build();
    }

    /**
     * Buscar Todos
     *
     * @return todos los registros tipoEstadoReserva
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("all")
    public List<TipoEstadoReserva> findAll() {
        List<TipoEstadoReserva> lista = new ArrayList<>();

        if (tipoEstadoReserva != null) {
            try {
                if (tipoEstadoReserva.findAll() != null) {
                    return lista = tipoEstadoReserva.findAll();
                }
            } catch (Exception e) {
            }

        }
        return lista = Collections.EMPTY_LIST;
    }

    /**
     * Buscar por un rango de registros
     *
     * @param primero
     * @param pagezise
     * @return
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<TipoEstadoReserva> findRange(
            @QueryParam("inicio") @DefaultValue("0") int primero,
            @QueryParam("cantidad") @DefaultValue("3") int pagezise) {

        List<TipoEstadoReserva> ls;
        if (tipoEstadoReserva != null) {
            try {

                return ls = tipoEstadoReserva.findRange(primero, pagezise);

            } catch (Exception e) {
            }

        }

        return ls = Collections.EMPTY_LIST;
    }

    /**
     * Buscar registros por iD
     *
     * @param identificador id a buscar
     * @return
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findId(@PathParam("id") Integer identificador) {

        TipoEstadoReserva buscado = tipoEstadoReserva.find(identificador);

        try {
            if (identificador >= 0 && buscado != null) {
                return Response.status(Response.Status.OK).entity(buscado).build();
            }
        } catch (Exception e) {
        }

        return Response.status(Response.Status.BAD_REQUEST).header("no se encontro registro:", identificador).build();
    }

    /**
     * Buscar coincidencias por nombre
     *
     * @param nombre parte del nombre a buscar
     * @return lista de registros que coinciden con la busqueda
     */
    @GET
    @Path("search")
    @Produces({MediaType.APPLICATION_JSON})
    public List<TipoEstadoReserva> searchLike(@QueryParam("name") String nombre) {
        List<TipoEstadoReserva> lista_ = null;
        if (tipoEstadoReserva != null) {

            return lista_ = tipoEstadoReserva.buscarPorNombre(nombre);
        }

        return lista_ = Collections.EMPTY_LIST;
    }

  
    
         /**
     * Crear registros
     * @param registro a crear 
     * @return la entidad creada
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public TipoEstadoReserva crear(TipoEstadoReserva registro){
       
        if(registro != null && registro.getIdTipoEstadoReserva()== null){
            try {
                if (tipoEstadoReserva != null) {
                    TipoEstadoReserva nuevo = tipoEstadoReserva.crear(registro);
                    if(nuevo!=null){
                        return nuevo;
                    }else{
                        System.err.println("facade nulo");
                    }
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new TipoEstadoReserva();
        
    }
    
    
    /**
     * Editar registros
     * @param registro, parametroa modificar
     * @return registro modificado
     */
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public TipoEstadoReserva editar(TipoEstadoReserva registro){
        if(registro != null){
            try {
                if (tipoEstadoReserva != null && tipoEstadoReserva.find(registro.getIdTipoEstadoReserva())!=null) {
                    TipoEstadoReserva nuevo = tipoEstadoReserva.editar(registro);
                    if(nuevo!=null){
                        return nuevo;
                    }
                }else{
                    System.out.println("no existe ese registro");
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new TipoEstadoReserva();
    }
    
    /**
     * Eliminar registros
     * @param id identificadir del registro a eliminar
     * @return entidad eliminada
     */
    @DELETE
    @Path("{idTipoEstadoReserva}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TipoEstadoReserva eliminar(@PathParam("idTipoEstadoReserva") int id){
        if(id > 0){
            try {
                if (tipoEstadoReserva != null) {
                    TipoEstadoReserva die = tipoEstadoReserva.remover(tipoEstadoReserva.find(id));
                    if(die!=null){
                        return die;
                    }
                }
            } catch (Exception e) {
                System.out.println("ex: "+e);
            }
        }
        return new TipoEstadoReserva();
    }
    
}
