/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso;

import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin
 */
@Stateless
public class TipoEstadoReservaFacade extends AbstractFacade<TipoEstadoReserva> implements TipoEstadoReservaFacadeLocal {

    @PersistenceContext(unitName = "flota_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoEstadoReservaFacade() {
        super(TipoEstadoReserva.class);
    }

    @Override
    public List<TipoEstadoReserva> buscarPorNombre(String nombre) {
        List<TipoEstadoReserva> myList;
        try {
            String q="SELECT a FROM TipoEstadoReserva a WHERE a.nombre like '"+nombre+"%'";
            Query query = getEntityManager().createQuery(q);
           // query.setParameter("nombre", nombre);
            return myList = query.getResultList();
        } catch (Exception e) {
        }
        return myList = Collections.EMPTY_LIST;
    }

}
